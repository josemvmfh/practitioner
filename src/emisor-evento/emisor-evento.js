import { LitElement, html } from "lit";

class EmisorEvento extends LitElement {
  static get properties() {
    return {};
  }
  constructor() {
    super();
  }
  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />
      <h3>emisor evento</h3>
      <button @click="${this.sendEvent}">botonaco</button>
    `;
  }
  sendEvent(e) {
    console.log("he pulsao");
    console.log(e);
    this.dispatchEvent(
      new CustomEvent("test-event", {
        detail: {
          course: "TechU",
          year: 2022,
        },
      })
    );
  }
}
customElements.define("emisor-evento", EmisorEvento);
