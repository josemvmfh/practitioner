import { LitElement, html } from "lit";
import "../persona-ficha-listado/persona-ficha-listado";
import "../persona-form/persona-form.js";
import "../persona-main-dm/persona-main-dm.js"

class PersonaMain extends LitElement {
  static get properties() {
    return {
      people: { type: Array },
      showPersonForm: { type: Boolean },
    };
  }
  constructor() {
    super();
    this.people = [];
    this.showPersonForm = false;
    this.maxYearsInCompanyFilter=0;
  }
  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />

      <h2 class="text-center">Personicas</h2>

      <div class="row" id="peopleList">
        <main class="row row-cols-1 row-cols-sm-4">
          ${this.people.map(
            (person) => html` <persona-ficha-listado
              fname="${person.name}"
              yearsInCompany="${person.yearsInCompany}"
              .photo="${person.photo}"
              profile="${person.profile}"
              @delete-person="${this.deletePerson2}"
              @info-person="${this.infoPersonMain}"
            ></persona-ficha-listado>`
          )}
          <!-- ese @ en el delete person es para llamar/esperar al evento personalizado delete person? -->
        </main>
      </div>
      <div class="row">
        <persona-form
          id="personForm"
          class="d-none border rounded border-primary"
          @persona-form-close="${this.personFormClose}"
          @persona-form-store="${this.personaFormStoreMain}"
        ></persona-form>

      </div>
 <persona-main-dm @updated-people="${this.updatePeople}"></persona-main-dm>
    `;
  }
  updated(changedProperties) {
    console.log("Updated al clic");
    if (changedProperties.has("showPersonForm")) {
      console.log("ha cambiado el showPersonForm en persona-main");
      if (this.showPersonForm === true) {
        this.showPersonFormData();
      } else {
        this.showPersonList();
      }
    }
    if (changedProperties.has("people")) {
      console.log("ha cambiado el people en persona-main");
      this.dispatchEvent(
        new CustomEvent(
          "people-updated",
          {
            detail:{
              people: this.people
            }
          }
        )
      )
    }

    console.log(changedProperties);
  }
  deletePerson2(e) {
    console.log("deletePerson2 en personaMain");
    console.log("se va a borrar la paersona de nombre " + e.detail.name);

    this.people = this.people.filter((person) => person.name != e.detail.name);
  }
  infoPersonMain(e) {
    console.log("infoPersonMain en personaMain");
    console.log("se ha pedido mas info de la persona" + e.detail.name);

    let chosenPerson = this.people.filter(
      (person) => person.name === e.detail.name
    );
    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;

    this.shadowRoot.getElementById("personForm").person = person;
    this.shadowRoot.getElementById("personForm").editingPerson = true;
    this.showPersonForm = true;
  }
  showPersonList() {
    console.log("showPersonList");
    console.log("showPersonList muestra el listado de personas");
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
  }
  showPersonFormData() {
    console.log("showPersonFormData");
    console.log("showPersonFormData muestra el formulario");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
  }
  personFormClose(e) {
    console.log("personFormClose");
    console.log("bonton atras recogido en main");
    this.showPersonForm = false;
  }
  personaFormStoreMain(e) {
    console.log("personaFormStoreMain");
    console.log(
      "recogemos el evento en personaFormStoreMain y lo manejamos con cariño"
    );

    console.log(e.detail);
    console.log(e.detail.person);

    if (e.detail.editingPerson === true) {
      console.log(
        "Se va a actualizar la persona con el nombre " + e.detail.person.name
      );
      // let indexOfPerson = this.people.findIndex(
      //   (person) => person.name === e.detail.person.name
      // );
      // if (indexOfPerson >= 0) {
      //   console.log("persona encontrada");
      //   this.people[indexOfPerson] = e.detail.person;
      // }
      this.people = this.people.map((person) =>
        person.name === e.detail.person.name
          ? (person = e.detail.person)
          : person
      );
    } else {
      console.log("se va a almacenar una persona nueva");
      this.people = [...this.people, e.detail.person];
    }

    console.log("persona guarrdada");
    this.showPersonForm = false;
  }
  updatePeople(e){
    console.log("updatedPeople en persona-main")
    this.people= e.detail.people
  }
}
customElements.define("persona-main", PersonaMain);
