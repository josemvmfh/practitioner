import { LitElement, html } from "lit";

class FichaPersona extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      yearsInCompany: { type: Number },
      personInfo: { type: String },
    };
  }
  constructor() {
    super();

    this.name = "Carlos no";
    this.yearsInCompany = 2;

    if (this.yearsInCompany >= 7) {
      this.personInfo = "lead";
    } else if (this.yearsInCompany >= 5) {
      this.personInfo = "senioe";
    } else if (this.yearsInCompany >= 3) {
      this.personInfo = "boss";
    } else {
      this.personInfo = "empleado";
    }
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      console.log(
        "Propiedad " + propName + " ha cambiado, antes era " + oldValue
      );
    });
    if (changedProperties.has("name")) {
      console.log(
        "el valor name ha cambiado, antes era " +
          changedProperties.get("name") +
          " el nuevo es " +
          this.name
      );
    }
    console.log(changedProperties);
  }
  render() {
    return html`
            <div>
                <label>
                    Nombre completo
                </label>        
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" value="${this.yearsInCompany}" @input="${this.updateInfo}" @change="${this.infochecker}"></input>
                <br />
                <input type="text" value="${this.personInfo}"  disabled></input>
                <br />      
            </div>
            `;
  }
  updateName(e) {
    console.log("updateao");
    //console.log(e);
    this.name = e.target.value;
  }
  updateInfo(e) {
    console.log("updateInfo");
    console.log(e);
    this.year;
    this.infochecker();
  }

  infochecker() {
    if (this.yearsInCompany >= 7) {
      this.personInfo = "lead";
    } else if (this.yearsInCompany >= 5) {
      this.personInfo = "seniore";
    } else if (this.yearsInCompany >= 3) {
      this.personInfo = "boss";
    } else {
      this.personInfo = "empleado";
    }
  }
}
customElements.define("ficha-persona", FichaPersona);
