import { LitElement, html } from "lit";

class TestApi extends LitElement {
  static get properties() {
    return {
      movies: { type: Array },
    };
  }
  constructor() {
    super();
    this.movies = [];
    this.getMovieData();
  }
  render() {
    return html` ${this.movies.map(
        movie => html` <div> la pelicula ${movie.title} fue dirigida por${movie.director} </div>`
    )}`;
    //pasa a ser movie porque movie es lo que esta dentro del json
  }
  getMovieData() {
    console.log("getMovieData");
    console.log("obteniendo datos de las pelis");
    let xhr = new XMLHttpRequest();
    xhr.onload = () => {
      if (xhr.status === 200) {
        console.log("200 ok");
        console.log(xhr.responseText)
        
        let APIresponse= JSON.parse(xhr.responseText)
        console.log(APIresponse)
        this.movies= APIresponse.results

      }
    };
    //
    xhr.open("GET", "https://swapi.dev/api/films");
    //promisse para sincrono
    xhr.send();
    //eso es un body
    console.log("Fin de getMovieData");
  }
}
customElements.define("test-api", TestApi);
