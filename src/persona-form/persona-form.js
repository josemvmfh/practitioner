import { LitElement, html } from "lit";

class PersonaForm extends LitElement {
  static get properties() {
    return {
      person: { type: Object },
      editingPerson: {type: Boolean}
    };
  }
  constructor() {
    super();
    this.resetFormData();
  }
  render() {
    return html`
      <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous"
      />
      <div>
        <form>
          <div class="form-group">
            <label> nombre completo </label>
            <input
              type="text"
              class="form-control"
              placeholder="Nombre Completo"
              @input="${this.updateName}"
              .value="${this.person.name}"
              ?disabled="${this.editingPerson}"
            />
          </div>
          <div class="form-group">
            <label> Perfil </label>
            <textarea
              class="form-control"
              placeholder="Perfil"
              rows="5"
              @input="${this.updateProfile}"
              .value="${this.person.profile}"
            >
            </textarea>
          </div>
          <div class="form-group">
            <label> Años en la empresa </label>
            <input
              type="number"
              class="form-control"
              placeholder="años en la empresa"
              @input="${this.updateYearsInCompany}"
              .value="${this.person.yearsInCompany}"
            />
          </div>
          <br />
          <button class="btn btn-primary" @click="${this.goBack}">
            <strong>Atras </strong>
          </button>
          <button class="btn btn-success" @click="${this.storePerson}">
            <strong>guardar </strong>
          </button>
        </form>
      </div>
    `;
  }
  goBack(e) {
    console.log("goBack");
    console.log("Se ha pulsado el boton de atras en persona form");
    e.preventDefault();

    this.dispatchEvent(new CustomEvent("persona-form-close", {}));
    this.resetFormData();
  }

  storePerson(e) {
    e.preventDefault();
    console.log("storePerson");
    console.log("Se ha pulsado el boton de guardar en persona form");

    console.log("La propiedad name vale " + this.person.name);
    console.log("La propiedad name vale " + this.person.profile);
    console.log("La propiedad name vale " + this.person.yearsInCompany);
    this.person.photo = {
      src: "./img/image.png",
      alt: "Persona",
    };

    this.dispatchEvent(
      new CustomEvent("persona-form-store", {
        detail: {
          person: {
            name: this.person.name,
            profile: this.person.profile,
            yearsInCompany: this.person.yearsInCompany,
            photo: this.person.photo,
          },
          editingPerson: this.editingPerson
        },
      })
    );
  }
  updateName(e) {
    console.log("updateName");
    console.log(
      "Actualizando la propiedad name con el valor " + e.target.value
    );
    this.person.name = e.target.value;
  }

  updateProfile(e) {
    console.log("updateProfile");
    console.log(
      "Actualizando la propiedad profile con el valor " + e.target.value
    );
    this.person.profile = e.target.value;
  }
  updateYearsInCompany(e) {
    console.log("updateYearsInCompany");
    console.log(
      "Actualizando la propiedad YearsInCompany con el valor " + e.target.value
    );
    this.person.yearsInCompany = e.target.value;
  }
  resetFormData() {
    console.log("resetFormData");
    this.person = {};
    this.person.name = "";
    this.person.profile = "";
    this.person.yearsInCompany = "";
    this.editingPerson=false;
  }
}
customElements.define("persona-form", PersonaForm);
