import { LitElement, html } from "lit";

class PersonaStats extends LitElement {
  static get properties() {
    return {
      people: { type: Array },
    };
  }
  constructor() {
    super();
    this.people = [];
  }
  updated(changedProperties) {
    console.log("updated en persona-stats");

    if (changedProperties.has("people")) {
      console.log(
        "ha cambiado el valor de la propiedad people ne PERSONA-STATS"
      );
      let peopleStats = this.gatherPeopleArrayInfo(this.people);
      this.dispatchEvent(
        new CustomEvent("updated-people-stats", {
          detail: {
            peopleStats: peopleStats,
          },
        })
      );
    }
  }
  gatherPeopleArrayInfo(people) {
    console.log("gatherpeoplearrayinfo");
    let peopleStats = {};
    peopleStats.numberOfPeople = people.length;
    console.log(peopleStats)
    return peopleStats;
  }
}
customElements.define("persona-stats", PersonaStats);
