import { LitElement, html } from "lit";

class PersonaMainDm extends LitElement {
  static get properties() {
    return {
      people: { type: Array },
    };
  }
  constructor() {
    super();
    this.people = [
      {
        name: "Persona 1",
        yearsInCompany: 10,
        photo: {
          src: "./img/image.png",
          alt: "Persona 1",
        },
        profile: "Palabras random del perfil coso 1",
      },
      {
        name: "Persona 2",
        yearsInCompany: 2,
        photo: {
          src: "./img/image.png",
          alt: "Persona 2",
        },
        profile: "Palabras random del perfil coso 2",
      },
      {
        name: "Persona 3",
        yearsInCompany: 1,
        photo: {
          src: "./img/image.png",
          alt: "Persona 3",
        },
        profile: "Palabras random del perfil coso 3",
      },
      {
        name: "Persona 4",
        yearsInCompany: 14,
        photo: {
          src: "./img/image.png",
          alt: "Persona 4",
        },
        profile: "Palabras random del perfil coso 4",
      },
      {
        name: "Persona 5",
        yearsInCompany: 12,
        photo: {
          src: "./img/image.png",
          alt: "Persona 5",
        },
        profile: "Palabras random del perfil coso 5",
      },
    ];
  }
  render() {
    return html` <h1>Persona header</h1> `;
  }
  updated(changedProperties) {
    if (changedProperties.has("people")) {
      console.log("pasa por el DM");
      this.dispatchEvent(
        new CustomEvent("updated-people", {
          detail: {
            people: this.people,
          },
        })
      );
    }
  }
}
customElements.define("persona-main-dm", PersonaMainDm);
